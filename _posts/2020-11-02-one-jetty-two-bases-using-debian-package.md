---
layout: post
title:  "One Jetty with two instances using Debian package"
date:   2020-11-02 10:10:00 +0200
categories: general
---

# One Jetty with two instances using Debian package

Instated of installing multiple Jetty binaries in different locations you can rather use Jetty capability to have a single binary installation (known as `jetty.home`) and multiple specific environments (known as `jetty.base`). This feature lets you easily manage multiple server instances.

In this tutorial we will see how you can use Jetty Debian 10 package to quickly run several independent instances.

This tutorialis is based on expertise from [Martin Hamant](https://gitlab.ow2.org/mh) and myself while we explore various deployment scenarios for hosting several web applications on a single server.

If you want to learn more about Jetty Base and Jetty Home refers to [Jetty official documentation](https://www.eclipse.org/jetty/documentation/current/startup-base-and-home.html).

## Install Jetty

First step is of course to install Jetty: `sudo apt install jetty9`

## Default location of Jetty Base and Jetty Home

When using Debian package, `/usr/share/jetty9` includes both Jetty Home and the default Jetty Base.

In the folder `/usr/share/jetty9` the following folders are actually part of Jetty Base:
- `start.ini` (symlink, actual location: `/etc/jetty9/start.ini`): define settings such as Jetty listening port, threads and timeout.
- `start.d` (symlink, actual location: `/etc/jetty9/start.d`): empty by default, can be used as an alternative to the single `start.ini` configuration file.
- `webapps` (symlink, actual location: `/var/lib/jetty9/webapps`): the folder to deploy your Java EE web applications.

In our setup we will not use the `jetty.base` located in `/usr/share/jetty9` nor the default `jetty9` Systemd service.

## Stop and disable `jetty9` Systemd service

`jetty9` service does not define `JETTY_BASE` leading Jetty to use the same location as Jetty Home for Jetty Base.

In order to avoid any confusion we need to stop and disable the `jetty9` Systemd service:
- `sudo systemctl stop jetty9`
- `sudo systemctl disable jetty9`

## Create new Jetty bases

In this tutorial we will create two Jetty Base. Jetty Base are simply folders initialized with some Jetty configuration files.

### Folders

Create two folders, e.g.:
- `mkdir /home/$USER/jetty-base-a`
- `mkdir /home/$USER/jetty-base-b`

### Initialization

We know need to initialize the folders created previously with some sub-folders and files.

For each folder do:
- Move to the folder (e.g. `cd /home/$USER/jetty-base-a`)
- Run `java -jar /usr/share/jetty9/start.jar --create-startd` to create the directory structure to store service configuration files.
- Run (adjust `jetty.base` path) `java -jar /usr/share/jetty9/start.jar --add-to-start=http,deploy,jsp,http-forwarded,console-capture jetty.base=/home/$USER/<BASE_FOLDER_NAME> jetty.home=/usr/share/jetty9` to enable services and add their configuration files to the `start.d` folder created by the previous command.

Note that you can add any [services](https://www.eclipse.org/jetty/documentation/current/startup-modules.html#startup-listing-modules) you need to the `--add-to-start` parameter.

### Set ownership and permissions

We will use the user named `jetty` created during installation of `jetty9` package to run both of newly created Jetty instances. 

For each folder created previously you will need to set `jetty` to be the owner and `adm` to be the group (this will mimic default Debian ownership set on `/var/lib/jetty9/`): `sudo chown jetty:adm /home/$USER/<BASE_FOLDER_NAME>`

You will need to apply same settings on `webapps` folder created in your Jetty Base when you enabled "deploy" module:   `sudo chown jetty:adm /home/$USER/<BASE_FOLDER_NAME>/webapps`

You might want to restrict access to log file:
- `sudo chown jetty:jetty /home/$USER/<BASE_FOLDER_NAME>/logs`
- `sudo chmod 750 /home/$USER/<BASE_FOLDER_NAME>/logs`

Finally, you might want to only allow `root` user to edit configuration files:  `sudo chown -R root:root /home/$USER/<BASE_FOLDER_NAME>/start.d`

## HTTP configuration

One minimal setting to edit in order to be able to run our two instances side by side is the HTTP listening port.

Edit `http.ini` located in `start.d` directory inside your Jetty Base, and add (or uncomment and customize) the following line: 
```
jetty.http.port=9999
```

Now you should have two properly configured Jetty Base. Next step is to be able to start two Jetty instance using those two Jetty Base with the help of systemd. 

## systemd

We will use systemd to start and stop our two Jetty instances.

First step is to duplicate the systemd Jetty service file provided by Debian package (one for each instance):
- `sudo cp /lib/systemd/system/jetty9.service /etc/systemd/system/jetty9-a.service`
- `sudo cp /lib/systemd/system/jetty9.service /etc/systemd/system/jetty9-b.service`

Now you can edit each file (e.g. `sudo nano /etc/systemd/system/jetty9-a.service`) and do the following:
- Update the service description: `Description=`.
- Comment out `Environment="JETTY_STATE=...."` line. If `JETTY_STATE` system variable is unset, the state file will be created in Jetty Base directory.
- Add `Environment="JETTY_BASE=/home/<YOUR_USERNAME>/<BASE_FOLDER_NAME>"` in `Configuration` section.
- Rename `JAVA_OPTS` to `JAVA_OPTIONS` if you want to define JVM options.
- Update `SyslogIdentifier`, e.g.: `SyslogIdentifier=jetty9-a` in `Logging` section.
- Update `LogsDirectory`, e.g.: `LogsDirectory=jetty9-a`.
- Update `ReadWritePaths`, e.g.: `ReadWritePaths=/home/<YOUR_USERNAME>/<BASE_FOLDER_NAME>`.

You are all set! Ready to launch your Jetty instances:
- `sudo systemctl start jetty9-a.service`
- `sudo systemctl start jetty9-b.service`
