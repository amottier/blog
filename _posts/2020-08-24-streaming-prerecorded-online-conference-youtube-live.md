---
layout: post
title:  "Streaming prerecorded online conference on YouTube live"
date:   2020-08-24 10:12:00 +0200
categories: general
---

In this article I'll explain how you can easily stream a prerecorded video on YouTube Live using open source software.

We use it at [OW2](https://www.ow2.org) for our own online conference: [OW2online](https://www.ow2con.org/view/2020/OW2online_Intro?year=2020&event=OW2con20) and as it did run flawlessly I thought it'd be a good idea to share my experience.

[![OW2online logo](https://www.ow2con.org/download/2020/WebHome/OW2online20-RVB_Evolution_279x120.png)](https://www.ow2con.org/view/2020/OW2online_Intro?year=2020&event=OW2con20)

## YouTube

Obviously the first step is to create an account on YouTube.

Next you need to create a new stream:

1. Go to YouTube Studio live: [https://studio.youtube.com/channel/UC/livestreaming](https://studio.youtube.com/channel/UC/livestreaming)
1. In the menu in the left click on "Stream" to create a new stream
1. Configure the new stream:
   * Title: choose a short meaningful title.
   * For visibility you have 3 options: public, private (only you) or unlisted (only users with the link).
   * Provide a description and make sure that it includes keywords that can help potential viewers to find your stream (if public).
   * Pick up a category.
   * You are probably planning your live event ahead so check the option "Schedule for later" and select a date and time.
   * Upload a thumbnail that will be displayed to users when they try to view your stream before it actually starts.
   * Select the appropriate option to define the targeted audience.
![Screenshot of YouTube stream configuration]({{site.baseurl}}/assets/img/youtube-studio-live-streaming.png)
1. Click on "Create stream" button.
1. All the default settings are usually just fine.
1. Copy the stream key for later use.

With that the YouTube part should be ready and now you can move on to the streaming part.

## Broadcast with Open Broadcaster Studio

Open Broadcaster Studio (OBS) is an open source software that will let you stream your prerecorded video content to YouTube. 

First you need to [download](https://obsproject.com/) and install OBS.

Once installed start OBS.

![OBS on first start]({{site.baseurl}}/assets/img/obs-first-start.png)

### Settings

On the first start you will be asked if you want to run the auto-configuration wizard.
Choose no, we will do a manual configuration.

![OBS first start waiting for confirmation if auto-configuration wizard should be executed]({{site.baseurl}}/assets/img/auto-configuration-no.png)

Click on the "Settings" button

![Location of OBS settings button]({{site.baseurl}}/assets/img/obs-settings-button.png)

and apply the following modifications (assuming you plan to stream a video recorded in Full HD 1080p without downgrading the quality):

* General:
  * Select "Show confirmation dialog when stopping streams". This will prevent you from accidentally stopping the stream.
* Stream:
  * Service: "YouTube".
  * Server: "Primary YouTube ingest server". You can stream same content from another computer as a backup. On this second computer you should choose "Backup YouTube ingest server".
  * Stream key: paste the key you copied when creating the stream in YouTube Studio.
* Output:
  * Output Mode: "Advanced".
  * Streaming tab:
    * Encoder: "x264".
    * Rate Control: "VBR".
    * Bitrate: 8000 Kbps.
  * Recording tab (in case you plan to also record locally your streaming):
    * Recording Format: "mp4".
* Audio:
  * Sample Rate: "48kHz".
  * Desktop Audio: "Disabled". You usually don't want to stream sounds coming from the OS.
  * Mic/Auxiliary Audio: "Disabled". This will prevent from streaming sound from your microphone over your prerecorded video.
* Video:
  * Base (Canvas) Resolution: 1920x1080 (this assume that your screen is a full HD).
  * Output (Scaled) Resolution: 1920x1080 (you want the Aspect Ratio to be 16:9 to match YouTube recommendation).
  * Common FPS Values: "30".
  
Click on "OK" to close settings window and on "Yes" to restart OBS.

### Content to stream

In order to control when our playback and stream will start we want to configure two "scenes". Each scene will include a single source.
The first scene will include a source with a static image (usually the same as the thumbnail configured in YouTube).
The second scene will include a source that is actually your video file.

In order to stream the static image you need to create and configure a "source" on the already existing default scene (named "Scene"):

* In the "Sources" area of OBS window click on the "+" icon.
* Select "Image" in the list of source types.
* Choose the option create a new source and keep the default name: "Image".
* Click on "OK" button.
* Click on the "Browse" button to select the image file to stream.
* Click on "OK" button.

![Adding a source (an image) to a scene]({{site.baseurl}}/assets/img/adding-image-to-scene.gif)

In order to stream a video you need to:

* Create a new scene by clicking on the "+" icon in the "Scenes" area of OBS window.
* Keep default name "Scene 2" and click on "OK" button.
* In the "Sources" area of OBS window click on the "+" icon.
* Select "Media Source" in the list of source types.
* Choose the option create a new source and keep the default name "Media Source".
* Click on "OK" button.
* Click on the "Browse" button to select the video file to stream.
* Select the option "Use hardware decoding when available" for better performance.
* Click on "OK" button.

![OBS ready for broadcast]({{site.baseurl}}/assets/img/obs-ready-for-broadcast.png)

### Streaming

And now the final steps to start your live stream:

1. Select the scene with your static image.
1. Click on "Start streaming" button.
1. In YouTube click on "Go live" button.
1. In OBS, select the scene with your video file.
1. You are streaming your video live!

## Conclusion

Doing live stream video event is quite easy and you can achieve professional results using exclusively open source software.

We use this kind of setup for [OW2online](https://www.ow2con.org/view/2020/OW2online_Intro?year=2020&event=OW2con20) live event. In an upcoming article I'll cover other aspect of the live event such as scheduling, participants registration and interaction with speaker.

If you need more information you can reach me on Twitter [@antoinemottier](https://twitter.com/antoinemottier/) or [LinkedIn](www.linkedin.com/in/antoine-mottier) and also checkout [OW2 activities](https://www.ow2.org).
